//
//  LoggerLevel.swift
//  alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import Foundation

enum LoggerLevel {
    case Trace
    case Debug
    case Log
    case Info
    case Warn
    case Error
    case Fatal
    case Analytics
}
