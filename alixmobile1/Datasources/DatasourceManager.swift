//
//  DatasourceManager.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import Foundation

class DatasourceManager {

	static let sharedInstance = DatasourceManager()
	
	lazy var EmptyDatasource: LocalDatasource<Item1> = {
    
        return LocalDatasource<Item1>(resource: DatasourceConfig.Local.EmptyDatasource.resource) 
    }()
	
	
	lazy var QAScreen1DS: CloudDatasource<QAScreen1DSItem> = {
    
        return CloudDatasource<QAScreen1DSItem>(baseUrl: DatasourceConfig.Cloud.baseUrl,
                                             resource: DatasourceConfig.Cloud.QAScreen1DS.resource,
                                             apikey: DatasourceConfig.Cloud.QAScreen1DS.apiKey,
                                             searchableFields: [QAScreen1DSItemMapping.filter, QAScreen1DSItemMapping.title, QAScreen1DSItemMapping.description]) 
    }()
	
	lazy var JobListingScreen1DS: CloudDatasource<JobListingScreen1DSItem> = {
    
        return CloudDatasource<JobListingScreen1DSItem>(baseUrl: DatasourceConfig.Cloud.baseUrl,
                                             resource: DatasourceConfig.Cloud.JobListingScreen1DS.resource,
                                             apikey: DatasourceConfig.Cloud.JobListingScreen1DS.apiKey,
                                             searchableFields: [JobListingScreen1DSItemMapping.filter, JobListingScreen1DSItemMapping.title, JobListingScreen1DSItemMapping.description]) 
    }()
	
	lazy var BestPracticesScreen1DS: CloudDatasource<BestPracticesScreen1DSItem> = {
    
        return CloudDatasource<BestPracticesScreen1DSItem>(baseUrl: DatasourceConfig.Cloud.baseUrl,
                                             resource: DatasourceConfig.Cloud.BestPracticesScreen1DS.resource,
                                             apikey: DatasourceConfig.Cloud.BestPracticesScreen1DS.apiKey,
                                             searchableFields: [BestPracticesScreen1DSItemMapping.filter, BestPracticesScreen1DSItemMapping.title, BestPracticesScreen1DSItemMapping.description]) 
    }()
	
	lazy var ContactDS: CloudDatasource<ContactDSItem> = {
    
        return CloudDatasource<ContactDSItem>(baseUrl: DatasourceConfig.Cloud.baseUrl,
                                             resource: DatasourceConfig.Cloud.ContactDS.resource,
                                             apikey: DatasourceConfig.Cloud.ContactDS.apiKey,
                                             searchableFields: [ContactDSItemMapping.whyContact, ContactDSItemMapping.contact, ContactDSItemMapping.call]) 
    }()
	
}
