//
//  DatasourceConfig.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import Foundation
 
 enum DatasourceConfig {

	
	enum Local {

		enum EmptyDatasource{
			
			static let resource = "EmptyDatasource"
		}
	}
	 
	
	enum Cloud {
		
		static let baseUrl = "https://ibm-pods.buildup.io"

		enum QAScreen1DS{
			
			static let resource = "/app/580ebc5b2b92fb03000c2096/r/qAScreen1DS"
			static let apiKey = "FB3iimnP"
		}

		enum JobListingScreen1DS{
			
			static let resource = "/app/580ebc5b2b92fb03000c2096/r/jobListingScreen1DS"
			static let apiKey = "FB3iimnP"
		}

		enum BestPracticesScreen1DS{
			
			static let resource = "/app/580ebc5b2b92fb03000c2096/r/bestPracticesScreen1DS"
			static let apiKey = "FB3iimnP"
		}

		enum ContactDS{
			
			static let resource = "/app/580ebc5b2b92fb03000c2096/r/contactDS"
			static let apiKey = "FB3iimnP"
		}
	}


}
