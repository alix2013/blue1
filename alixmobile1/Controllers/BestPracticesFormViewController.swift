//
//  BestPracticesFormViewController.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//
import UIKit

class BestPracticesFormViewController: FormViewController {
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

		AnalyticsManager.sharedInstance?.analytics?.logPage("")
		let item = self.item as? BestPracticesScreen1DSItem
    
		let filterField = StringField(name: BestPracticesScreen1DSItemMapping.filter, label: "Filter", required: false, value: item?.filter)
        contentView.addSubview(filterField)
		
		let titleField = StringField(name: BestPracticesScreen1DSItemMapping.title, label: "Title", required: false, value: item?.title)
        contentView.addSubview(titleField)
		
		let pictureField = ImageField(name: BestPracticesScreen1DSItemMapping.picture, label: "Picture", required: false, viewController: self, value: crudService?.imagePath(item?.picture))
        contentView.addSubview(pictureField)
		
		let descriptionField = StringField(name: BestPracticesScreen1DSItemMapping.description, label: "Description", required: false, value: item?.description)
        contentView.addSubview(descriptionField)
		
       
        updateViewConstraints()
        
        retrieveFields()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
