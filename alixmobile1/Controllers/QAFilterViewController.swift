//
//  QAFilterViewController.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import UIKit

class QAFilterViewController: FilterViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterFields = [
			OptionsFilterField(datasource: datasource,
                datasourceOptions: datasourceOptions,
                name: QAScreen1DSItemMapping.filter,
                label: "Filter",
                viewController: self),
            
        ]

        loadFields()
    }
}
