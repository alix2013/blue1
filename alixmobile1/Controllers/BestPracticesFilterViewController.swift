//
//  BestPracticesFilterViewController.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import UIKit

class BestPracticesFilterViewController: FilterViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterFields = [
			OptionsFilterField(datasource: datasource,
                datasourceOptions: datasourceOptions,
                name: BestPracticesScreen1DSItemMapping.filter,
                label: "Filter",
                viewController: self),
			OptionsFilterField(datasource: datasource,
                datasourceOptions: datasourceOptions,
                name: BestPracticesScreen1DSItemMapping.title,
                label: "Title",
                viewController: self),
			OptionsFilterField(datasource: datasource,
                datasourceOptions: datasourceOptions,
                name: BestPracticesScreen1DSItemMapping.description,
                label: "Description",
                viewController: self),
            
        ]

        loadFields()
    }
}
