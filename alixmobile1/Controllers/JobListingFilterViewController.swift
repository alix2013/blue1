//
//  JobListingFilterViewController.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import UIKit

class JobListingFilterViewController: FilterViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filterFields = [
			OptionsFilterField(datasource: datasource,
                datasourceOptions: datasourceOptions,
                name: JobListingScreen1DSItemMapping.filter,
                label: "Filter",
                viewController: self),
            
        ]

        
        loadFields()
    }
}
