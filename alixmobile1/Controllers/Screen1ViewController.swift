//
//  Screen1ViewController.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import UIKit

class Screen1ViewController: CollectionViewController<Item1>, UICollectionViewDelegate, UICollectionViewDataSource {
    
    typealias Cell = PhotoCell
    
    override init() {
        super.init()
		
		datasource = DatasourceManager.sharedInstance.EmptyDatasource
		
		dataResponse = self
		
		behaviors.append(SearchBehavior(viewController: self))
			
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AnalyticsManager.sharedInstance?.analytics?.logPage("screen1")
        title = NSLocalizedString("screen1", comment: "")
		
		collectionView.delegate = self
        collectionView.dataSource = self
        collectionView?.registerClass(Cell.self, forCellWithReuseIdentifier: Cell.identifier)
		
		numberOfColumns = 3

		updateViewConstraints()
		
        loadData()
		
		behaviors.append(SearchBehavior(viewController: self))
		

    }
    
    // MARK: <UICollectionViewDatasource>
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return items.count
    }
	
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Cell.identifier, forIndexPath: indexPath) as! Cell

		// No detail or actions
        cell.userInteractionEnabled = false
	
        
        
        // Binding 
        let item = items[indexPath.row]

		// Styles
		
		return cell
    }


}

//MARK: - <DataResponse>

extension Screen1ViewController: DataResponse {

    func success() {
        
        collectionView?.reloadData()
    }
    
    func failure(error: NSError?) {
        
        ErrorManager.show(error, rootController: self)
    }
}
