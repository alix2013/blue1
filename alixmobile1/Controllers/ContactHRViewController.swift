//
//  ContactHRViewController.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import UIKit

class ContactHRViewController: DataViewController<ContactDSItem> {
    
    var didSetupConstraints = false
   
	var imageUrl1: UIImageView!
	var titleLabel2Header: UILabel!
	var titleLabel2: UILabel!
	var titleLabel3Header: UILabel!
	var titleLabel3: UILabel!
	var titleLabel3Button: UIButton!
	
	var titleLabel4Header: UILabel!
	var titleLabel4: UILabel!
	var titleLabel4Button: UIButton!
	
	
	
	override init() {
        super.init()
        
        datasource = DatasourceManager.sharedInstance.ContactDS
        
        dataResponse = self

	
    }
	
	override func viewDidLoad() {
        super.viewDidLoad()

		AnalyticsManager.sharedInstance?.analytics?.logPage("Contact HR")
		title = NSLocalizedString("Contact HR", comment: "")
		

		imageUrl1 = createImageView()
		addSubview(imageUrl1)	
		
		titleLabel2Header = createHeaderLabel("WHY CONTACT HR")
		addSubview(titleLabel2Header)	
		
		titleLabel2 = createLabel()
		addSubview(titleLabel2)	
		
		titleLabel3Header = createHeaderLabel("CONTACT HR")
		addSubview(titleLabel3Header)	
		
		titleLabel3 = createLabel()
		addSubview(titleLabel3)	
		
		titleLabel3Button = createButton(Images.location)
        addSubview(titleLabel3Button)
		 
		titleLabel4Header = createHeaderLabel("CALL HR")
		addSubview(titleLabel4Header)	
		
		titleLabel4 = createLabel()
		addSubview(titleLabel4)	
		
		titleLabel4Button = createButton(Images.phone)
        addSubview(titleLabel4Button)
		 
	
		
		updateViewConstraints()
		
		loadData()
	}
    
    override func updateViewConstraints() {
        
        if !didSetupConstraints {
            
            didSetupConstraints = true
            setupConstraints()
        }
        super.updateViewConstraints()
    }

	func setupConstraints() {
        
        let views:[String: UIView] = [
            "scrollView": scrollView,
            "contentView": contentView,
			"imageUrl1": imageUrl1,
			"titleLabel2Header": titleLabel2Header, 
			"titleLabel2": titleLabel2,
			"titleLabel3Header": titleLabel3Header, 
			"titleLabel3": titleLabel3,
			"titleLabel3Button": titleLabel3Button,
			"titleLabel4Header": titleLabel4Header, 
			"titleLabel4": titleLabel4,
			"titleLabel4Button": titleLabel4Button,
        ]
        
        let metrics:[String: CGFloat] = [
            "zero": Dimens.Margins.none,
            "margin": Dimens.Margins.large,
            "buttonSize": Dimens.Sizes.small
        ]
		
		contentView.removeConstraints(contentConstraints)
        scrollView.removeConstraints(scrollConstraints)
        view.removeConstraints(mainConstraints)
		
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		mainConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
        
        mainConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollView]|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
        
		contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView(==scrollView)]|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
        
        scrollConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
			
		imageUrl1.translatesAutoresizingMaskIntoConstraints = false
		contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|[imageUrl1]|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
        
        contentConstraints.append(NSLayoutConstraint(item: imageUrl1,
            attribute: .CenterX,
            relatedBy: .Equal,
            toItem: contentView,
            attribute: .CenterX,
            multiplier: 1.0,
            constant: 0))
		titleLabel2Header.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[titleLabel2Header]-margin-|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
			
		titleLabel2.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[titleLabel2]-margin-|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
		
		titleLabel3Header.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[titleLabel3Header]-margin-|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
			
		titleLabel3.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[titleLabel3]-margin-[titleLabel3Button(buttonSize)]-margin-|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
		
		titleLabel3Button.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("V:[titleLabel3Button(buttonSize)]",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
		
		contentConstraints.append(NSLayoutConstraint(item: titleLabel3Button,
            attribute: .CenterY,
            relatedBy: .Equal,
            toItem: titleLabel3,
            attribute: .CenterY,
            multiplier: 1.0,
            constant: 0))
			
		titleLabel4Header.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[titleLabel4Header]-margin-|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
			
		titleLabel4.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[titleLabel4]-margin-[titleLabel4Button(buttonSize)]-margin-|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
		
		titleLabel4Button.translatesAutoresizingMaskIntoConstraints = false
        contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("V:[titleLabel4Button(buttonSize)]",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))
		
		contentConstraints.append(NSLayoutConstraint(item: titleLabel4Button,
            attribute: .CenterY,
            relatedBy: .Equal,
            toItem: titleLabel4,
            attribute: .CenterY,
            multiplier: 1.0,
            constant: 0))
			
		contentConstraints.appendContentsOf(NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[imageUrl1]-margin-[titleLabel2Header]-margin-[titleLabel2]-margin-[titleLabel3Header]-margin-[titleLabel3]-margin-[titleLabel4Header]-margin-[titleLabel4]-(>=margin)-|",
            options: .DirectionLeadingToTrailing,
            metrics: metrics,
            views: views))

		view.addConstraints(mainConstraints)
        contentView.addConstraints(contentConstraints)
        scrollView.addConstraints(scrollConstraints)
	}
}

extension ContactHRViewController: DataResponse {

    func success() {
 		imageUrl1.loadImage(datasource.imagePath(item?.picture), containerView: view)
		
		titleLabel2.text = item?.whyContact
		
		titleLabel3.text = item?.contact
		
		setAction(MapAction(uri: item?.contact), view: titleLabel3Button)
		 
		titleLabel4.text = item?.call
		
		setAction(PhoneAction(uri: item?.call), view: titleLabel4Button)
		 
       
    }
    
    func failure(error: NSError?) {
        ErrorManager.show(error, rootController: self)
    }
}

