//
//  JobListingScreen1DSItem.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import Foundation

enum JobListingScreen1DSItemMapping {
	static let filter = "filter"
	static let title = "title"
	static let picture = "picture"
	static let description = "description"
	static let id = "id"
}

public class JobListingScreen1DSItem : Item {
	
	var filter : String?
	var title : String?
	var picture : String?
	var description : String?
	var id : String?
	
	// MAR: - <Item>

	public var identifier: AnyObject? {
		guard let identifier = id else {
			return ""
		}
		return identifier
	}
	
	required public init?(dictionary: NSDictionary?) {
    
        retrieve(dictionary)
    }
	
	public func retrieve(dictionary: NSDictionary?) {
     
        guard let dic = dictionary else {
            return
        }
        
 		filter = dic[JobListingScreen1DSItemMapping.filter] as? String
		
		title = dic[JobListingScreen1DSItemMapping.title] as? String
		
		picture = dic[JobListingScreen1DSItemMapping.picture] as? String
		
		description = dic[JobListingScreen1DSItemMapping.description] as? String
		
		id = dic[JobListingScreen1DSItemMapping.id] as? String
		
	   
    }
}
	
