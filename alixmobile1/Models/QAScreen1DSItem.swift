//
//  QAScreen1DSItem.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import Foundation

enum QAScreen1DSItemMapping {
	static let filter = "filter"
	static let title = "title"
	static let picture = "picture"
	static let description = "description"
	static let order = "order"
	static let id = "id"
}

public class QAScreen1DSItem : Item {
	
	var filter : String?
	var title : String?
	var picture : String?
	var description : String?
	var order : Int?
	var id : String?
	
	// MAR: - <Item>

	public var identifier: AnyObject? {
		guard let identifier = id else {
			return ""
		}
		return identifier
	}
	
	required public init?(dictionary: NSDictionary?) {
    
        retrieve(dictionary)
    }
	
	public func retrieve(dictionary: NSDictionary?) {
     
        guard let dic = dictionary else {
            return
        }
        
 		filter = dic[QAScreen1DSItemMapping.filter] as? String
		
		title = dic[QAScreen1DSItemMapping.title] as? String
		
		picture = dic[QAScreen1DSItemMapping.picture] as? String
		
		description = dic[QAScreen1DSItemMapping.description] as? String
		
		order = dic[QAScreen1DSItemMapping.order] as? Int
		
		id = dic[QAScreen1DSItemMapping.id] as? String
		
	   
    }
}
	
