//
//  ContactDSItem.swift
//  Alixmobile1
//
//  This App has been generated using IBM Mobile App Builder
//

import Foundation

enum ContactDSItemMapping {
	static let whyContact = "whyContact"
	static let contact = "contact"
	static let picture = "picture"
	static let call = "call"
	static let id = "id"
}

public class ContactDSItem : Item {
	
	var whyContact : String?
	var contact : String?
	var picture : String?
	var call : String?
	var id : String?
	
	// MAR: - <Item>

	public var identifier: AnyObject? {
		guard let identifier = id else {
			return ""
		}
		return identifier
	}
	
	required public init?(dictionary: NSDictionary?) {
    
        retrieve(dictionary)
    }
	
	public func retrieve(dictionary: NSDictionary?) {
     
        guard let dic = dictionary else {
            return
        }
        
 		whyContact = dic[ContactDSItemMapping.whyContact] as? String
		
		contact = dic[ContactDSItemMapping.contact] as? String
		
		picture = dic[ContactDSItemMapping.picture] as? String
		
		call = dic[ContactDSItemMapping.call] as? String
		
		id = dic[ContactDSItemMapping.id] as? String
		
	   
    }
}
	
